class CreateTasks < ActiveRecord::Migration[6.0]
  def change
    create_table :tasks do |t|
      t.references :list, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.string :title, null: false
      t.integer :status, null: false, default: 0
      t.integer :chain, null: false, default: 0

      t.timestamps
    end
    add_index :tasks, :status
    add_index :tasks, :chain
  end
end
