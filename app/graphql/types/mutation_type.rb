module Types
  class MutationType < Types::BaseObject
    # field :signUp, mutation: Mutations::SignUp
    field :sign_up, mutation: Mutations::Auth::SignUp
    field :sign_in, mutation: Mutations::Auth::SignIn
    field :destroy_user, mutation: Mutations::Auth::DestroyUser
    field :create_list, mutation: Mutations::Lists::CreateList
    field :update_list, mutation: Mutations::Lists::UpdateList
    field :destroy_list, mutation: Mutations::Lists::DestroyList
    field :create_task, mutation: Mutations::Tasks::CreateTask
    field :update_task, mutation: Mutations::Tasks::UpdateTask
    field :destroy_task, mutation: Mutations::Tasks::DestroyTask
    field :change_status_task, mutation: Mutations::Tasks::ChangeStatusTask
  end
end
