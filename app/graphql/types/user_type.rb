module Types
  class UserType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: true
    field :email, String, null: true
    field :created_at, String, null: true
    field :updated_at, String, null: true
    field :lists, [Types::ListType], null: false
  end
end
