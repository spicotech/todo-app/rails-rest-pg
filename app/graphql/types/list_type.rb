module Types
  class ListType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: true
    field :created_at, String, null: true
    field :updated_at, String, null: true
    field :tasks, [Types::TaskType], null: false
    field :user, Types::UserType, null: true
  end
end
