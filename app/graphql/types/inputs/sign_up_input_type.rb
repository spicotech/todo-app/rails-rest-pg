module Types
  module Inputs
    class SignUpInputType < BaseInputObject
      # the name is usually inferred by class name but can be overwritten
      # graphql_name 'SignUpInputType'

      argument :name, String, required: true
      argument :email, String, required: true
      argument :password, String, required: true
    end
  end
end