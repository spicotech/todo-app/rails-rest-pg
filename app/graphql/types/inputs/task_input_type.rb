module Types
  module Inputs
    class TaskInputType < BaseInputObject
      argument :title, String, required: true
    end
  end
end