module Types
  module Inputs
    class ListInputType < BaseInputObject
      argument :name, String, required: true
    end
  end
end