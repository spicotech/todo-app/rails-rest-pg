module Types
  module Inputs
    class SignInInputType < BaseInputObject
      argument :email, String, required: true
      argument :password, String, required: true
    end
  end
end