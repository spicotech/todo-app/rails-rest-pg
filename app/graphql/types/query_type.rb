module Types
  class QueryType < Types::BaseObject
    field :me, Types::UserType, null: false

    def me
      authenticate_user
      context[:current_user]
    end

    field :lists, [Types::ListType], null: false

    def lists
      authenticate_user
      context[:current_user].lists
    end

    field :list, Types::ListType, null: false do
      argument :id, ID, required: true
    end

    def list(id:)
      authenticate_user
      context[:current_user].lists.find(id)
    end

    field :tasks, [Types::TaskType], null: false do
      argument :list_id, ID, required: true
    end

    def tasks(list_id:)
      authenticate_user
      context[:current_user].lists.find(list_id).tasks
    end

    field :task, Types::TaskType, null: false do
      argument :list_id, ID, required: true
      argument :id, ID, required: true
    end

    def task(list_id:, id:)
      authenticate_user
      list = context[:current_user].lists.find(list_id)
      list.tasks.find(id)
    end

    def authenticate_user
      unless context[:current_user]
        return GraphQL::ExecutionError.new(I18n.t('errors.unauthenticated'))
      end
    end
  end
end
