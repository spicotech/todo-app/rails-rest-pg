module Types
  class TaskType < Types::BaseObject
    field :id, ID, null: false
    field :title, String, null: true
    field :status, String, null: true
    field :chain, Int, null: false
    field :created_at, String, null: true
    field :updated_at, String, null: true
    field :list, Types::ListType, null: true
    field :user, Types::UserType, null: true
  end
end
