module Mutations
  module Auth
    class SignUp < BaseMutation
      require 'json_web_token'

      argument :input, Types::Inputs::SignUpInputType, required: true

      field :token, String, null: false
      field :user, Types::UserType, null: false

      def resolve(input: nil)
        user = User.new(name: input.name, email: input.email, password: input.password)

        if user.save
          token = JsonWebToken.encode({user_id: user.id})
          if token
            return {user: user, token: token}
          else
            GraphQL::ExecutionError.new(I18n.t('errors.token.not_created'))
          end
        else
          GraphQL::ExecutionError.new(user.errors.full_messages.join(', '))
        end
      end
    end
  end
end
