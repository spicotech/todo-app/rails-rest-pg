module Mutations
  module Auth
    class DestroyUser < BaseMutation

      type Boolean

      def resolve
        authenticate_user
        context[:current_user].destroy!
        true
      end
    end
  end
end
