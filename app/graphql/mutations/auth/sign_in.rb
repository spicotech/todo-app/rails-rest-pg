module Mutations
  module Auth
    class SignIn < BaseMutation
      require 'json_web_token'

      argument :input, Types::Inputs::SignInInputType, required: true

      field :token, String, null: false
      field :user, Types::UserType, null: false

      def resolve(input: nil)
        user = User.find_by(email: input.email)
        return GraphQL::ExecutionError.new(I18n.t('errors.controllers.auth.invalid_credentials')) if user.nil?
        if user.authenticate(input.password)
          token = JsonWebToken.encode({user_id: user.id})
          if token
            {token: token, user: user}
          else
            GraphQL::ExecutionError.new(I18n.t('errors.token.not_created'))
          end
        else
          GraphQL::ExecutionError.new(user.errors.full_messages.join(', '))
        end
      end
    end
  end
end
