module Mutations
  # This class is used as a parent for all mutations, and it is the place to have common utilities
  class BaseMutation < GraphQL::Schema::Mutation
    null false

    def authenticate_user
      unless context[:current_user]
        return GraphQL::ExecutionError.new(I18n.t('errors.unauthenticated'))
      end
    end
  end
end