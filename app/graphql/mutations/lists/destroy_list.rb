module Mutations
  module Lists
    class DestroyList < BaseMutation
      argument :id, String, required: true

      type Boolean

      def resolve(id: nil)
        authenticate_user
        list = context[:current_user].lists.find(id)
        list.destroy!
        true
      end
    end
  end
end