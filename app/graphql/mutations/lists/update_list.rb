module Mutations
  module Lists
    class UpdateList < BaseMutation
      argument :id, String, required: true
      argument :input, Types::Inputs::ListInputType, required: true

      type Types::ListType

      def resolve(id: nil, input: nil)
        authenticate_user
        list = context[:current_user].lists.find(id)
        if list.update(name: input.name)
          list
        else
          GraphQL::ExecutionError.new(list.errors.full_messages.join(', '))
        end
      end
    end
  end
end