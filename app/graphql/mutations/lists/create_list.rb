module Mutations
  module Lists
    class CreateList < BaseMutation

      argument :input, Types::Inputs::ListInputType, required: true

      type Types::ListType

      def resolve(input: nil)
        authenticate_user
        list = context[:current_user].lists.build(name: input.name)
        if list.save
          list
        else
          GraphQL::ExecutionError.new(list.errors.full_messages.join(', '))
        end
      end
    end
  end
end
