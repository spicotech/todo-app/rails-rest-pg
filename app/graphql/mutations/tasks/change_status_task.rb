module Mutations
  module Tasks
    class ChangeStatusTask < BaseMutation

      argument :list_id, String, required: true
      argument :id, String, required: true

      type Types::TaskType

      def resolve(list_id: nil, id: nil)
        authenticate_user
        list = context[:current_user].lists.find(list_id)
        task = list.tasks.find(id)
        task.change_status!
        task
      end
    end
  end
end
