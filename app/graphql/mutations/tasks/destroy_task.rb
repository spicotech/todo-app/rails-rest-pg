module Mutations
  module Tasks
    class DestroyTask < BaseMutation

      argument :list_id, String, required: true
      argument :id, String, required: true

      type Boolean

      def resolve(list_id: nil, id: nil)
        authenticate_user
        list = context[:current_user].lists.find(list_id)
        task = list.tasks.find(id)
        task.destroy!
        true
      end
    end
  end
end
