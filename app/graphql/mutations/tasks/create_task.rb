module Mutations
  module Tasks
    class CreateTask < BaseMutation

      argument :list_id, String, required: true
      argument :input, Types::Inputs::TaskInputType, required: true

      type Types::TaskType

      def resolve(list_id: nil, input: nil)
        authenticate_user
        list = context[:current_user].lists.find(list_id)
        task = list.tasks.build(title: input.title, user: context[:current_user])
        if task.save
          task
        else
          GraphQL::ExecutionError.new(task.errors.full_messages.join(', '))
        end
      end
    end
  end
end
