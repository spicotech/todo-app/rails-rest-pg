module Mutations
  module Tasks
    class UpdateTask < BaseMutation

      argument :list_id, String, required: true
      argument :id, String, required: true
      argument :input, Types::Inputs::TaskInputType, required: true

      type Types::TaskType

      def resolve(list_id: nil, id: nil, input: nil)
        authenticate_user
        list = context[:current_user].lists.find(list_id)
        task = list.tasks.find(id)
        if task.update(title: input.title)
          task
        else
          GraphQL::ExecutionError.new(task.errors.full_messages.join(', '))
        end
      end
    end
  end
end
