class User < ApplicationRecord
  ## SCHEMA
  # create_table "users", force: :cascade do |t|
  #   t.string "name"
  #   t.string "email", null: false
  #   t.string "password_digest"
  #   t.datetime "created_at", precision: 6, null: false
  #   t.datetime "updated_at", precision: 6, null: false
  # end

  ## ENTITIES
  has_secure_password

  ## VALIDATIONS
  validates :name, presence: true
  validates :email, presence: true, uniqueness: true, format: {with: /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i, message: I18n.t("errors.models.user.format_email")}
  validates :password, format: {with: /\A(?=.*[a-zA-Z])(?=.*[0-9]).{8,72}\z/, message: I18n.t("errors.models.user.format_password")}, if: lambda { password_digest.nil? || !password.blank? }

  ## CALLBACKS
  before_validation :downcase_email!

  ## RELATIONSHIPS
  has_many :lists, dependent: :destroy
  has_many :tasks, dependent: :destroy

  private

  # downcase email
  def downcase_email!
    email&.downcase!
  end
end
