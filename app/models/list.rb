class List < ApplicationRecord
  ## SCHEMA
  # create_table "lists", force: :cascade do |t|
  #   t.bigint "user_id", null: false
  #   t.string "name", null: false
  #   t.datetime "created_at", precision: 6, null: false
  #   t.datetime "updated_at", precision: 6, null: false
  # end

  ## RELATIONSHIPS
  belongs_to :user
  has_many :tasks, dependent: :destroy

  ## VALIDATIONS
  validates :name, presence: true
end
