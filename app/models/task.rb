class Task < ApplicationRecord
  ## SCHEMA
  # create_table "tasks", force: :cascade do |t|
  #   t.bigint "list_id", null: false
  #   t.bigint "user_id", null: false
  #   t.string "title", null: false
  #   t.integer "status", default: 0, null: false
  #   t.integer "chain", default: 0, null: false
  #   t.datetime "created_at", precision: 6, null: false
  #   t.datetime "updated_at", precision: 6, null: false
  # end

  ## ENTITIES
  extend Enumerize
  enumerize :status, in: {todo: 0, done: 1}

  ## RELATIONSHIPS
  belongs_to :list
  belongs_to :user

  ## VALIDATIONS
  validates :title, presence: true
  validates :status, presence: true
  # chain => for sequence of the list tasks
  validates :chain, presence: true

  ## CALLBACKS
  before_validation :initialize_object, on: :create

  # initialize task with initial values
  def initialize_object
    self.status = :todo
    self.chain = if Task.maximum(:chain).nil?
                   1
                 else
                   Task.maximum(:chain) + 1
                 end
  end

  # change the status to task
  def change_status!
    if status == 'todo'
      update(status: 'done')
    else
      update(status: 'todo')
    end
  end
end
