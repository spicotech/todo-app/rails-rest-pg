class Rest::V1::Auth::RegistrationsController < ApplicationController
  include CreateSession
  before_action :authenticate_user, only: :destroy

  def create
    @user = User.new registration_params

    if @user.save
      @token = jwt_session_create @user.id
      if @token
        return success_user_created
      else
        return error_token_create
      end
    else
      return error_user_save
    end
  end

  def destro
    current_user.destroy!
    return success_user_destroy
  end

  protected

  def success_user_created
    response.headers['Authorization'] = "Bearer #{@token}"
    render status: :created, template: 'rest/v1/users/auth.json.jbuilder'
  end

  def error_token_create
    render status: :unprocessable_entity, json: {errors: [I18n.t('errors.token.not_created')]}
  end

  def error_user_save
    render status: :unprocessable_entity, json: {errors: @user.errors.full_messages}
  end

  def success_user_destroy
    render status: :no_content, json: {}
  end

  private

  def registration_params
    params.permit(:email, :password, :name)
  end
end