class Rest::V1::TasksController < ApplicationController
  before_action :authenticate_user
  before_action :set_list
  before_action :set_task, only: [:update, :destroy, :show, :status]

  def create
    @task = @list.tasks.build(task_params)
    @task.user = current_user

    if @task.save
      success_task_create
    else
      error_task_save
    end
  end

  def update
    if @task.update(task_params)
      success_task_save
    else
      error_task_save
    end
  end

  def destroy
    @task.destroy
    success_task_destroy
  end

  def show
    success_task_save
  end

  def index
    @tasks = @list.tasks.order(chain: :desc)
    success_task_index
  end

  def status
    @task.change_status!
    success_task_save
  end

  protected

  def success_task_create
    render status: :created, template: 'rest/v1/tasks/show.json.jbuilder'
  end

  def success_task_save
    render status: :ok, template: 'rest/v1/tasks/show.json.jbuilder'
  end

  def success_task_destroy
    render status: :no_content, json: {}
  end

  def success_task_index
    render status: :ok, template: 'rest/v1/tasks/index.json.jbuilder'
  end

  def error_task_save
    render status: :unprocessable_entity, json: {errors: @task.errors.full_messages}
  end

  private

  def set_list
    @list = current_user.lists.find(params[:list_id])
  end

  def task_params
    params.permit(:title)
  end

  def set_task
    @task = @list.tasks.find(params[:id])
  end
end