class Rest::V1::ListsController < ApplicationController
  before_action :authenticate_user
  before_action :set_list, only: [:update, :show, :destroy]

  def create
    @list = current_user.lists.build(list_params)

    if @list.save
      success_list_create
    else
      error_list_save
    end
  end

  def update
    if @list.update(list_params)
      success_list_save
    else
      error_list_save
    end
  end

  def show
    success_list_save
  end

  def destroy
    @list.destroy
    success_list_destroy
  end

  def index
    @lists = current_user.lists.order(id: :desc)
    success_list_index
  end

  protected

  def success_list_create
    render status: :created, template: 'rest/v1/lists/show.json.jbuilder'
  end

  def success_list_save
    render status: :ok, template: 'rest/v1/lists/show.json.jbuilder'
  end

  def success_list_destroy
    render status: :no_content, json: {}
  end

  def success_list_index
    render status: :ok, template: 'rest/v1/lists/index.json.jbuilder'
  end

  def error_list_save
    render status: :unprocessable_entity, json: {errors: @list.errors.full_messages}
  end

  private

  def list_params
    params.permit(:name)
  end

  def set_list
    @list = current_user.lists.find(params[:id])
  end
end