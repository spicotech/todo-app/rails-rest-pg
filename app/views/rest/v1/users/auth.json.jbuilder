json.Authorization "Bearer #{@token}"
json.user do
  json.partial! 'rest/v1/users/self_user.json.jbuilder', user: @user
end