source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.5'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '6.0.1'
# Use postgresql as the database for Active Record
gem 'pg', '1.1.4'
# Use Puma as the app server
gem 'puma', '4.2.1'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '2.9.1'
# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '1.4.5', require: false
# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors', '1.0.3'
# for ENV variables
gem 'figaro', '1.1.1'
# for enum in integer columns
gem 'enumerize', '2.3.1'
# Use ActiveModel has_secure_password
gem 'bcrypt', '3.1.13'
# for jwt authentication token
gem 'jwt', '2.2.1'
# for error tracking
gem 'honeybadger', '~> 4.0'
gem 'listen', '3.2.0'
# graphql support
gem 'graphql', '1.9.15'
gem 'graphiql-rails', '1.7.0'
gem 'sprockets', '~> 3'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', '11.0.1', platforms: [:mri, :mingw, :x64_mingw]
  # testing suite
  gem 'rspec-rails', '4.0.0.beta3'
  gem 'factory_bot_rails', '5.1.1'
end

group :development do
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring', '2.1.0'
  gem 'spring-watcher-listen', '2.0.1'
  gem 'spring-commands-rspec', '1.0.4'
  # for deployment
  gem 'capistrano', '3.11.2'
  gem 'capistrano-rails', '1.4.0'
  gem 'capistrano-rbenv', '2.1.4'
  gem 'capistrano-passenger', '0.2.0'
  gem 'capistrano-rails-console', '2.3.0'
  gem 'airbrussh', '1.4.0'
  gem 'bcrypt_pbkdf', '1.0.1'
  gem 'ed25519', '1.2.4'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', '1.2019.3', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
