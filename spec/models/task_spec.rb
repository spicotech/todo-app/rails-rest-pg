require 'rails_helper'

RSpec.describe Task, type: :model do
  it "has a valid factory" do
    task = FactoryBot.build(:task)
    expect(task.valid?).to be_truthy
  end

  it "correctly increments the chain" do
    task1 = FactoryBot.create(:task)
    task2 = FactoryBot.create(:task, list_id: task1.list_id, user_id: task1.user_id)
    expect(task2.chain).to eql task1.chain + 1
  end
end
