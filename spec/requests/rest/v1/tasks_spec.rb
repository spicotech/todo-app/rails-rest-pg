require 'rails_helper'

RSpec.describe "Rest::V1::Tasks", type: :request do
  describe 'POST /rest/v1/lists/:list_id/tasks' do
    let(:user) { FactoryBot.create(:user) }
    let(:list) { FactoryBot.create(:list, user_id: user.id) }
    it "creates the task for user in given list" do
      headers = sign_in_test_headers user
      body = FactoryBot.attributes_for :task
      post rest_v1_list_tasks_path(list_id: list.id), headers: headers, params: body
      expect(response).to have_http_status 201
    end
  end

  describe 'PUT /rest/v1/lists/:list_id/tasks/:id' do
    let(:task) { FactoryBot.create(:task) }
    it "updates the tasks for the user" do
      headers = sign_in_test_headers task.user
      body = FactoryBot.attributes_for :task
      put rest_v1_list_task_path(list_id: task.list_id, id: task.id), headers: headers, params: body
      expect(response).to have_http_status 200
    end
  end

  describe 'GET /rest/v1/lists/:list_id/tasks/:id' do
    let(:task) { FactoryBot.create(:task) }
    it "shows the task of the user for given list" do
      headers = sign_in_test_headers task.user
      get rest_v1_list_task_path(list_id: task.list_id, id: task.id), headers: headers
      expect(response).to have_http_status 200
    end
  end

  describe 'DELETE /rest/v1/lists/:list_id/tasks/:id' do
    let(:task) { FactoryBot.create(:task) }
    it "deletes the task of the user for given list" do
      headers = sign_in_test_headers task.user
      delete rest_v1_list_task_path(list_id: task.list_id, id: task.id), headers: headers
      expect(response).to have_http_status 204
    end
  end

  describe 'GET /rest/v1/lists/:list_id/tasks' do
    let(:task) { FactoryBot.create(:task) }
    it "get all the tasks for given list" do
      headers = sign_in_test_headers task.user
      get rest_v1_list_tasks_path(list_id: task.list_id), headers: headers
      expect(response).to have_http_status 200
    end
  end

  describe 'PUT /rest/v1/lists/:list_id/tasks/:id/status' do
    let(:task) { FactoryBot.create(:task) }
    it "update the task of the user for given list" do
      headers = sign_in_test_headers task.user
      put status_rest_v1_list_task_path(list_id: task.list_id, id: task.id), headers: headers, params: {}
      expect(response).to have_http_status 200
      json = JSON.parse(response.body)
      expect(json['status']).to eql 'done'
    end
  end
end