require 'rails_helper'

RSpec.describe "Rest::V1::Lists", type: :request do
  describe 'POST /rest/v1/lists' do
    let(:user) { FactoryBot.create(:user) }
    it "creates the list for the user" do
      headers = sign_in_test_headers user
      body = FactoryBot.attributes_for :list
      post rest_v1_lists_path, params: body, headers: headers
      expect(response).to have_http_status(201)
    end

    it "gives error if user not signed in" do
      body = FactoryBot.attributes_for :list
      post rest_v1_lists_path, params: body
      expect(response).to have_http_status(401)
    end
  end

  describe 'PUT /rest/v1/lists/:id' do
    let(:list) { FactoryBot.create(:list) }
    it "updates the user list" do
      headers = sign_in_test_headers(list.user)
      body = FactoryBot.attributes_for :list
      put rest_v1_list_path(list), params: body, headers: headers
      expect(response).to have_http_status(200)
    end
  end

  describe 'GET /rest/v1/lists/:id' do
    let(:list) { FactoryBot.create(:list) }
    it "shows the user list" do
      headers = sign_in_test_headers(list.user)
      get rest_v1_list_path(list), headers: headers
      expect(response).to have_http_status(200)
    end
  end

  describe 'DELETE /rest/v1/lists/:id' do
    let(:list) { FactoryBot.create(:list) }
    it "deletes the user list" do
      headers = sign_in_test_headers(list.user)
      delete rest_v1_list_path(list), headers: headers
      expect(response).to have_http_status(204)
    end
  end

  describe 'GET /rest/v1/lists/' do
    let(:list) { FactoryBot.create(:list) }
    it "shows the user lists" do
      headers = sign_in_test_headers(list.user)
      get rest_v1_lists_path, headers: headers
      expect(response).to have_http_status(200)
    end
  end
end