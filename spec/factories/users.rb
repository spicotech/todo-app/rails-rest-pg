FactoryBot.define do
  factory :user do
    name { "Sulman Baig" }
    sequence(:email) { |n| "sulmanweb#{n}@gmail.com" }
    password { "abcd@1234" }
  end
end
