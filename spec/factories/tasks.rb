FactoryBot.define do
  factory :task do
    title { "MyString" }
    status { "todo" }
    chain { 1 }
    after(:build, :stub) do |task|
      user = FactoryBot.create(:user)
      task.user = user
      task.list = FactoryBot.create(:list, user_id: user.id)
    end
  end
end
