set :stage, :production
set :rails_env, :production
set :deploy_to, "/home/deploy/apis/todo-rails-api"
set :branch, "master"
# set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }
# set :whenever_roles, %w{web app db}
server "206.189.91.234", user: "deploy", roles: %w[web app db]
